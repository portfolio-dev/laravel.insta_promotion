<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NovoServices extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'novo_services';

    protected $fillable = [
        'service',
        'category',
        'max',
        'min',
        'name',
        'rate',
        'type',
    ];
}
