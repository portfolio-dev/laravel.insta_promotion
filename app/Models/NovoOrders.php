<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NovoOrders extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'novo_orders';

    protected $fillable = [
        'id',
        'link',
        'quantity',
        'service_id',
        'short_profile',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function service()
    {
        return $this->belongsTo(NovoServices::class, 'service_id', 'service');
    }

    /**
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param int $id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOfService($query, int $id)
    {
        return $query->where('service_id', $id);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return String
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @return int
     */
    public function getServiceId(): int
    {
        return $this->service_id;
    }

}
