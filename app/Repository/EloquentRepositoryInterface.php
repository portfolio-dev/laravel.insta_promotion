<?php

namespace App\Repository;

use Illuminate\Database\Eloquent\Model;

interface EloquentRepositoryInterface
{
    /**
     * @param array $params
     * @return Model|null
     */
    public function create(array $params): ?Model;
}
