<?php

declare(strict_types=1);

namespace App\Repository\Eloquent;

use App\Models\NovoOrders;
use App\Repository\NovoOrderRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class NovoOrderRepository extends BaseRepository implements NovoOrderRepositoryInterface
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * NovoOrderRepository constructor.
     * @param NovoOrders $model
     */
    public function __construct(NovoOrders $model)
    {
        $this->model = $model;
    }
}
