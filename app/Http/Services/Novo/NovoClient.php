<?php

declare(strict_types=1);

namespace App\Http\Services\Novo;

use Illuminate\Support\Facades\Http;

class NovoClient
{
    /** @var mixed $url */
    protected static $url = 'http://45.147.176.76:8910/v1/just';

    /** @var mixed $key */
    private static $key = '911863916d19759ccc873d427e563f223f0ee099de9b47997784eb9aeb03e06d';

    /**
     * Список Услуг
     *
     * @return array
     */
    public static function getServicesList(): array
    {
        /** @var \Illuminate\Http\Client\Response $response */
        $response = Http::get(self::$url, [
            'action'    => 'services',
            'key'       => self::$key,
        ]);

        return json_decode((string) $response, true);
    }

    /**
     * Создание заказа
     *
     * @param string $link
     * @param int $quantity
     * @param int $serviceId
     * @param int $operationId
     * @return array
     */
    public static function addOrder(string $link, int $quantity, int $serviceId, int $operationId): array
    {
        /** @var \Illuminate\Http\Client\Response $response */
        $response = Http::asForm()->post(self::$url . '/add?action=add&key=' . self::$key, [
            'link'          => $link,
            'quantity'      => $quantity,
            'service'       => $serviceId,
            'operation_id'  => $operationId
        ]);

        return json_decode((string) $response, true);
    }

    /**
     * Статус заказа
     *
     * @param int $orderId
     * @return array
     */
    public static function getStatusOrder(int $orderId): array
    {
        /** @var \Illuminate\Http\Client\Response $response */
        $response = Http::get(self::$url, [
            'action'    => 'status',
            'key'       => self::$key,
            'order'     => $orderId
        ]);

        return json_decode((string) $response, true);
    }
}
