<?php

declare(strict_types=1);

namespace App\Http\Services\Instagram;

use Illuminate\Support\Facades\Http;

class InstaClient
{
    /** @var mixed $url */
    protected static $url = 'https://www.instagram.com';

    /**
     * Instagram профиль пользователя
     *
     * @param string $link
     * @return \Illuminate\Http\Client\Response
     */
    public static function getUserProfile(string $link)
    {
        /** @var array $parse */
        $parse = parse_url($link);

        /** @var string $username */
        $username = str_replace('/', '', $parse['path']);

        return Http::withOptions(['verify' => false])->get(self::$url . "/$username/?__a=1");
    }

    /**
     * Краткая информация о профиле пользователя
     *
     * @param string $profile
     * @return array
     */
    public static function parseUserProfile(string $profile): array
    {
        /** @var array $profile */
        $profile = json_decode($profile, true);

        if (empty($profile)) {
            return [];
        }

        $result['full_name']    = $profile['graphql']['user']['full_name'];
        $result['username']     = $profile['graphql']['user']['username'];
        $result['profile_pic']  = 'data:image/jpg;base64,'.base64_encode(file_get_contents($profile['graphql']['user']['profile_pic_url']));

        return $result;
    }
}
