<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\LinkRequest;
use App\Http\Services\Instagram\InstaClient;
use App\Models\NovoServices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Response;

class InstagramController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * @param LinkRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function check(LinkRequest $request)
    {
        $data = $request->only('link');

        $userProfile    = InstaClient::getUserProfile($data['link']);
        $shortProfile   = InstaClient::parseUserProfile((string)$userProfile);
        $services       = NovoServices::query()->get();

        return Response::json([
            'profile'   => $shortProfile,
            'services'  => $services,
        ]);
    }
}
