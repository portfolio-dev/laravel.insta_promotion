<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\QuantityRequest;
use App\Http\Services\Novo\NovoClient;
use App\Models\NovoOrders;
use App\Repository\NovoOrderRepositoryInterface;
use Illuminate\Support\Facades\Response;

class NovoOrdersController extends Controller
{
    /** @var NovoOrderRepositoryInterface */
    private $novoOrderRepository;

    public function __construct(NovoOrderRepositoryInterface $novoOrderRepository)
    {
        $this->middleware('guest');
        $this->novoOrderRepository = $novoOrderRepository;
    }

    /**
     * @param QuantityRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function order(QuantityRequest $request)
    {
        $data = $request->only('followers', 'link', 'service_id', 'short_profile', 'quantity');

        $order  = $this->novoOrderRepository->create($data);
        $apply  = NovoClient::addOrder($order->getLink(), $order->getQuantity(), $order->getServiceId(), $order->getId());
        $status = NovoClient::getStatusOrder($apply['order']);

        $result['order']    = $order;
        $result['provider'] = $apply;
        $result['status']   = $status;

        return Response::json($result);
    }
}
