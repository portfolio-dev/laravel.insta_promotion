<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNovoOrdersTable extends Migration
{
    /** @var string $table */
    private $table = 'novo_orders';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->id();

            $table->string('link', 255)->nullable();
            $table->unsignedInteger('quantity')->nullable();
            $table->unsignedBigInteger('service_id')->nullable();
            $table->text('short_profile')->nullable();
            $table->timestamps();
            $table->foreign('service_id', 'novo_orders_service_id_foreign')->references('service')->on('novo_services');

            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
