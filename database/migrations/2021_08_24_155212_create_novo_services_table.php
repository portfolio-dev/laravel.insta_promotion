<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNovoServicesTable extends Migration
{
    /** @var string $table */
    private $table = 'novo_services';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('service')->nullable();
            $table->string('category', 100)->nullable();
            $table->unsignedInteger('max')->default(0);
            $table->unsignedInteger('min')->default(0);
            $table->string('name', 255)->nullable();
            $table->unsignedInteger('rate')->default(0);
            $table->string('type', 100)->nullable();
            $table->timestamps();
            $table->unique(['service']);

            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
