<?php

namespace Database\Seeders;

use App\Http\Services\Novo\NovoClient;
use App\Models\NovoServices;
use Illuminate\Database\Seeder;

class NovoServicesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = NovoClient::getServicesList();

        foreach ($items as $item) {
            NovoServices::create([
                'service'   => $item['service'],
                'category'  => $item['category'],
                'max'       => $item['max'],
                'min'       => $item['min'],
                'name'      => $item['name'],
                'rate'      => $item['rate'],
                'type'      => $item['type'],
            ]);
        }
    }
}
