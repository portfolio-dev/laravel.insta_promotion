@extends('layouts.app')

@section('content')
    <div class="max-w-6xl mx-auto sm:px-6 lg:px-8 bg-white">
        <div class="mt-8 container-md">
            <h2>Продвижение профиля в социальной сети Instagram</h2>
        </div>
        <div class="mt-8 first-section">
            <form class="container-md">
                <div class="mb-3">
                    <input type="text" class="form-control" id="insta-link" aria-describedby="linkHelp" placeholder="Ссылка на профиль в социальной сети" />
                    <div id="linkHelp" class="form-text">например, https://www.instagram.com/redbull/</div>
                </div>
                <div class="mb-3">
                    <div class="alert alert-danger error-link"></div>
                </div>
                <div class="d-flex justify-content-center">
                    <div class="spinner-border visually-hidden" role="status" id="loading">
                    </div>
                </div>
            </form>
        </div>

        <div class="mt-md-3 second-section">
            <div class="container-md">
                <div class="row">
                    <div class="col-12 profile-info"></div>
                </div>
            </div>

            <form class="container-md">
                <div class="row">
                    <div class="col-12 mb-4 service-group"></div>
                    <div class="col-12 mx-2 alert alert-danger error-message"></div>
                    <div class="col-4">
                        <input type="number" min="10" class="form-control" id="quantity" placeholder="Укажите необходимое кол-во подписчиков" />
                    </div>
                    <div class="col-3">
                        <button type="submit" class="btn btn-primary mb-3 add-order">Заказать</button>
                    </div>
                </div>
            </form>
        </div>


        <div class="mt-md-3 third-section">
            <div class="container-md">
                <div class="row">
                    <div class="col-12 order-title"></div>
                </div>
                <div class="row">
                    <div class="col-6 profile-info-duplicate"></div>
                </div>
                <div class="row">
                    <div class="col-6 status"></div>
                </div>
            </div>
        </div>
    </div>
@endsection
