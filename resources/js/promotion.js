jQuery(document).ready(function($){
    $('.add-order').on('click', function (e) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            }
        });

        e.preventDefault();

        $('.error-message').html('').css('display', 'none');

        let formData = {
            link: $('#insta-link').val(),
            quantity: $('#quantity').val(),
            service_id: $('input[name="srv"]:checked').val(),
            short_profile: $('.profile-info').attr('data-short-profile'),
        };

        $.ajax({
            type: 'POST',
            url: 'order',
            data: formData,
            dataType: 'json',
            success: function (data) {
                $('.order-title').html('<h3>Заказ #' + data.provider.order+ '</h3>');
                $('.profile-info-duplicate').html($('.profile-info').html());

                let count = data.status.start_count;
                if (!$.isNumeric(count)) {
                    count = 0;
                }

                $('.status').html('<span>' + count + ' из ' + data.status.remains + '</span><span class="price">' + data.status.charge + ' руб.</span>');
            },
            error: function (reject) {
                if( reject.status === 400 ) {
                    $('.error-message').css('display', 'block');

                    let response = $.parseJSON(reject.responseText);
                    $.each(response.errors, function (key, val) {
                        $('.error-message').append(val);
                    });
                }
            },
         });
    });
});
