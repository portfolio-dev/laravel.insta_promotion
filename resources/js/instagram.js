jQuery(document).ready(function($){
    function defaultOptions() {
        $('.second-section').css('display', 'none');
        $('.error-link').html('').css('display', 'none');
        $('.profile-info').html('');
        $('.service-group').html('');
    }

    function ajaxProfile(link) {
        let formData = {
            link: link,
        };

        $.ajax({
            type: 'POST',
            url: 'check',
            data: formData,
            dataType: 'json',
            beforeSend () {
                $('#loading').removeClass('visually-hidden');
            },
            success: function (data) {
                if (data.profile.length === 0) {
                    $('.error-link').css('display', 'block');
                    $('.error-link').append('Профиль не найден');

                    return;
                }

                let instaProfile = '';
                let radioSrv = '';

                instaProfile += "<img class='img-thumbnail' src='" + data.profile.profile_pic + "' />";
                instaProfile += "<span>" + data.profile.full_name + " (" + data.profile.username + ")</span>";

                $.each(data.services, function(key, value) {
                    let checked = key === 1 ? 'checked' : '';

                    radioSrv += "<div class='form-check'>";
                    radioSrv += "<input class='form-check-input' type='radio' name='srv' value='" + value.service + "' id='srv-" + value.service + "' " + checked + "/>";
                    radioSrv += "<label class='form-check-label' for='srv-" + value.service + "'>";
                    radioSrv += value.name;
                    radioSrv += ' (' + value.rate + ' руб. за 1000)';
                    radioSrv += "</label>";
                    radioSrv += "</div>";
                });

                $('.profile-info').attr('data-short-profile', JSON.stringify(data.profile));
                $('.profile-info').append(instaProfile);
                $('.service-group').append(radioSrv);
                $('.second-section').css('display', 'block');
            },
            error: function (reject) {
                if( reject.status === 400 ) {
                    $('.error-link').css('display', 'block');

                    let response = $.parseJSON(reject.responseText);
                    $.each(response.errors, function (key, val) {
                        $('.error-link').append(val);
                    });
                }
            },
            complete: function(){
                $('#loading').addClass('visually-hidden');
            },
        });
    }

    $('#insta-link').on('keypress', function(e) {
        if (e.keyCode == 13) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                }
            });

            e.preventDefault();

            defaultOptions();
            ajaxProfile($(this).val());

            return false;
        }
    });

    $('#insta-link').on('change', function (e) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            }
        });

        e.preventDefault();

        defaultOptions();
        ajaxProfile($(this).val());
    });
});
